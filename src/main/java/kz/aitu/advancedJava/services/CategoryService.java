package kz.aitu.advancedJava.services;

import kz.aitu.advancedJava.model.Category;
import kz.aitu.advancedJava.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Iterable<Category> findAll(){
        return categoryRepository.findAll();
    }

    public Optional<Category> findById(long id) { return  categoryRepository.findById(id); }

}
