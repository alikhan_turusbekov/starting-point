CREATE TABLE products (
    id        serial CONSTRAINT products_primary PRIMARY KEY,
    name       varchar(40) NOT NULL,
    price      int4 NOT NULL,
    description  varchar(255) NOT NULL,
    category_id  int8 NOT NULL,
    barcode      varchar(32) NOT NULL UNIQUE
);

CREATE TABLE categories (
    id        serial CONSTRAINT categories_primary PRIMARY KEY,
    name       varchar(40) NOT NULL,
    description  varchar(255) NOT NULL
);

