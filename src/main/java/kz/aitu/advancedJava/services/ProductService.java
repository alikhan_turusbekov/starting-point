package kz.aitu.advancedJava.services;

import kz.aitu.advancedJava.model.Category;
import kz.aitu.advancedJava.model.Product;
import kz.aitu.advancedJava.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Iterable<Product> findAll(){
        return productRepository.findAll();
    }

    public Optional<Product> findById(long id) { return  productRepository.findById(id); }

}
