package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.services.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/api/products/")
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(productService.findAll());
    }

    @GetMapping("/api/products/{id}")
    public void deleteById(@PathVariable long id) {
        productService.findById(id);
    }

}
